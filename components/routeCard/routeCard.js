'use strict';

import React, { Component, View, Text, StyleSheet, Image, ListView} from 'react-native';
import Dimensions from 'Dimensions';
const {width, height} = Dimensions.get('window');

class RouteCard extends Component {
	
  constructor() {
    super();
    this.state = {
      passedPlatforms: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
      startPlatform: '',
      finishPlatform: '',
      routeName: '',
      transportType: ''
    };
  }

  fetchData() {
    fetch('http://eta2.navi.ostack.test/vehicle_eta', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        project: 'spb',
        vehicle_id: this.props.vehicle_id,
      })
    }).then((response) => response.json())
      .then((data) => {
        console.log(data);
        this.setState({
          passedPlatforms: this.state.passedPlatforms.cloneWithRows(data.response.passed_platforms),
          startPlatform: data.response.start_platform,
          finishPlatform: data.response.finish_platform,
          transportType: data.response.transport_type,
          routeName: data.response.route_name
        });
      })
      .done();
  }

  componentDidMount() {
    this.fetchData();
  }

	renderPassedPlatforms(platform) {

	  return (
	      <View style={styles.container}>
	      	   
        		<View style={styles.rightContainer}>
          			<Text style={styles.title}>{platform.name}</Text>
        		</View>
	      </View>
	    );
  	}

  	renderLoadingView() {
      return (
        <View style={styles.container}>
          <Text>
            Загрузка маршрута...
          </Text>
        </View>
      );
  }

	render() {
    console.log(this.props);
		if(!this.props.vehicle_id) {
			return this.renderLoadingView();
		}
		
		return (
			<View style={styles.wrapper}>
				<Text>{this.state.routeName}</Text>
				<ListView
		        dataSource={this.state.passedPlatforms}
		        renderRow={this.renderPassedPlatforms}
		        style={styles.listView}/>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	wrapper: {
		width: width,
		height: 200,
		backgroundColor: 'orange',
	},
	container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  title: {
    fontSize: 20,
    marginBottom: 8,
    textAlign: 'center',
  },
	listView: {
    	paddingTop: 20,
    	backgroundColor: '#F5FCFF',
  },
});

export default RouteCard;