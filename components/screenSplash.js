'use strict';

import Button from 'react-native-button';
import React, { Component, View, Text, StyleSheet, Image, AsyncStorage } from 'react-native';
import Carousel from 'react-native-spring-carousel';
import LinearGradient from 'react-native-linear-gradient';
import Dimensions from 'Dimensions';
const {width, height} = Dimensions.get('window');

import LocalStorage from '../js/stores/LocalStorage';

const pages = [{
	title: 'Поехали',
	text: 'Приложение для поиска проезда на транспорте с учётом фактического времени прибытия и пробок',
	linearGradient: {
		from: '#FFFFFF',
		to: '#FFFFFF'
	},
	imageSource: require('./screenSplash/img/wizard_0.png')
}, {
	title: 'Транспорт',
	text: 'Наземный городской транспорт в реальном времени на самой точной карте города',
	linearGradient: {
		from: '#D1FF00',
		to: '#90B035'
	},
	imageSource: require('./screenSplash/img/wizard_1.png')
}, {
	title: 'Остановки',
	text: 'Фактическое время прибытия автобусов, троллейбусов, трамваев и маршруток для каждой остановки',
	linearGradient: {
		from: '#B7DFFF',
		to: '#549CEA'
	},
	imageSource: require('./screenSplash/img/wizard_2.png')
}, {
	title: 'Маршруты (скоро)',
	text: 'Поиск проезда на наземном транспорте с учётом времени прибытия, расписания и ситуации на дорогах',
	linearGradient: {
		from: '#FFF2BB',
		to: '#EDBC3E'
	},
	imageSource: require('./screenSplash/img/wizard_3.png')
}, {
	title: 'Напоминания',
	text: 'Push-сообщения, напоминающие заранее о приближении нужного транспорта на остановку',
	linearGradient: {
		from: '#FFD4B8',
		to: '#EB6060'
	},
	imageSource: require('./screenSplash/img/wizard_4.png'),
	btn: true
}];

class ScreenSplash extends Component {
	onPressBtn() {
		console.log('asdasas');
		LocalStorage.addToAsyncStorage('text');
	}

	render() {
		this.pagesList = pages.map((page, index) => {
			var btn;
			if (page.btn) {
				btn = <Button
						containerStyle={{
							margin: 18,
							paddingTop:13,
							height:48,
							width: 228,
							overflow:'hidden',
							borderRadius:4,
							backgroundColor: '#fff'}}
						style={{fontSize: 14, color: '#333', lineHeight: 20}}
						onPress={this.onPressBtn.bind(this)}
						>
						ПОЕХАЛИ
					</Button>;
			}
			return (
				<LinearGradient key={index} colors={[page.linearGradient.from, page.linearGradient.to]} style={styles.page}>
					{page.imageSource && <Image source={page.imageSource} />}
					{page.title && <Text style={styles.title}>{page.title}</Text>}
					{page.text && <Text style={styles.text}>{page.text}</Text>}
					{btn}
				</LinearGradient>
			);
		});

        return (
			<Carousel
				width={width}
				height={height}
				pagerColor="#AAA"
				activePagerColor="#333"
				pagerSize={10}
				pagerOffset={30}
				pagerMargin={2}
				>
				{this.pagesList}
            </Carousel>
        );
      }
}

const styles = StyleSheet.create({
	page: {
		width: width,
		height: height,
		alignItems: 'center',
		justifyContent: 'center',
	},
	title: {
		fontSize: 28,
		lineHeight: 35,
		color: '#000',
	},
	text: {
		fontSize: 14,
		lineHeight: 20,
		color: '#000',
		width: 296,
		textAlign: 'center',
		marginTop: 12,
	}
});

export default ScreenSplash;