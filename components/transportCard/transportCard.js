'use strict';

import React, { Component, View, Text, StyleSheet, Image, ListView} from 'react-native';
import Dimensions from 'Dimensions';
const {width, height} = Dimensions.get('window');

class TransportCard extends Component {
	
   constructor() {
    super();
    this.state = {
      routes: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
      platformName: ''
    };
  }

  componentWillUpdate() {
    this.fetchData();
  }
  componentDidMount() {
    this.fetchData();
  }

  fetchData() {
    fetch('http://eta2.navi.ostack.test/platform_eta', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        project: 'spb',
        platform_id: this.props.id_station,
      })
    }).then((response) => response.text())
      .then((responseText) => {
        this.setState({
          routes: this.state.routes.cloneWithRows(JSON.parse(responseText).response.routes),
          platformName: JSON.parse(responseText).response.platform_name
        });
      })
      .done();
  }

	renderRouter(router) {
    switch(router.transport_type) {
      case 'trolleybus': {
        router.transport_type = 'Троллейбус';
      }
      case 'shuttle_bus': {
        router.transport_type = 'Маршрутка';
      }
    }

	    return (
	      <View style={styles.container}>
          <View style={styles.rightContainer}>
            <Text style={styles.title}>{router.name}</Text>
          </View>
        	<View style={styles.rightContainer}>
          		<Text style={styles.transport_type}>{router.transport_type}</Text>
              <Text>{router.start_platform.name}</Text>
              <Text>{router.finish_platform.name}</Text>
        	</View>
          <View style={styles.rightContainer}>
              <Text style={styles.eta}>{router.eta} мин</Text>
          </View>
          <View>
            <Image source={require('./img/alarm_icon.png')} />
          </View>
	      </View>
	    );
  	}

  	renderLoadingView() {
    return (
      <View style={styles.container}>
        <Text>
          Загружаются маршруты...
        </Text>
      </View>
    );
  }

	render() {
		if(!this.state.platformName) {
			return this.renderLoadingView();
		}
		
		return (
			<View style={styles.wrapper}>
				<Text style={styles.cardTitle}>{this.state.platformName}</Text>
				<ListView
		        dataSource={this.state.routes}
		        renderRow={this.renderRouter}
		        style={styles.listView}/>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	wrapper: {
		width: width,
    backgroundColor: '#FFF',
	},
  cardTitle: {
    fontSize: 24,
    lineHeight: 30,
    margin: 20,
    color: '#3674B9'
  },
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginLeft: 20,
    marginRight: 20,
    marginTop: 12,
    marginBottom: 16,
  },
  rightContainer: {
    flex: 1,
  },
  title: {
    fontSize: 14,
    lineHeight: 20,
    color: '#333',
    fontWeight: 'bold',
    textAlign: 'left',
  },
  transport_type: {
    fontSize: 14,
    lineHeight: 20,
    color: '#333',
    textAlign: 'left',
  },
  eta: {
    fontSize: 14,
    lineHeight: 20,
    color: '#727272',
    textAlign: 'center',
  },
  thumbnail: {
    width: 53,
    height: 81,
  },
});

export default TransportCard;