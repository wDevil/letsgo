'use strict';

import React, { Component,WebView, View, Text, StyleSheet, Image } from 'react-native';
import WebViewBridge from 'react-native-webview-bridge';
import LocalStorage from '../../js/stores/LocalStorage';


class WebViewContainer extends Component {
	onBridgeMessage(message) {
		const { webviewbridge } = this.refs;

		if(message.url.search("#") == -1) return;
		
		var parsedMessage = message.url.substr(message.url.search("#")+5, message.url.length);
		LocalStorage.msgFromMap(JSON.parse(parsedMessage));
	}
	
	//тестовая отправка сообщения. Типа что-то прилетело из карты
	componentDidMount() {
		//LocalStorage.msgFromMap(JSON.parse('{"type": "stationClick", "data":{"id_station": 5348707457303608}}'));
		//LocalStorage.msgFromMap(JSON.parse('{"type": "vehicleClick", "data":{"id_vehicle": 9071}}'));
	}

	render() {
		return (
			<WebView
	          automaticallyAdjustContentInsets={false}
	          style={{height: 500}}
	          source={require('../../map/index.html')}
	          onNavigationStateChange={this.onBridgeMessage.bind(this)}
	          startInLoadingState={false}
	          javaScriptEnabled={true}
	          domStorageEnabled={true}
        	/>
		);
	}
}

const injectScript = `
 (function () {
   if (WebViewBridge) {
   	window.WebViewBridge = WebViewBridge;
     WebViewBridge.onMessage = function (message) {
       if (message === "hello from react-native") {
         WebViewBridge.send("got the message inside webview");
       }
     };

     WebViewBridge.send("hello from webview");
   }
 }());
`;

export default WebViewContainer;