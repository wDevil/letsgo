/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

import React, {
  AsyncStorage,
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  ListView,
  View
} from 'react-native';

import ScreenSplash from './components/screenSplash';
import TransportCard from './components/transportCard/transportCard';
import RouteCard from './components/routeCard/routeCard';
import WebViewContainer from './components/webViewContainer/webViewContainer';
import LocalStorage from './js/stores/LocalStorage';
import ParallaxView from 'react-native-parallax-view';

class eta extends Component {

  constructor() {
    super();
    this.state = {};
  }

  componentWillMount() {
    //AsyncStorage.setItem("isSplashScreenShown", "");

    LocalStorage.on('change', () => {
      this.setState({isSplashScreenShown: true});
      console.log(this.state);
    });

    LocalStorage.on('msgMap', (data) => {
      switch(data.type) {
        case "stationClick":
          this.setState({showStationCard: true, id_station: data.data.id_station});
        break;

         case "vehicleClick":
          this.setState({showRouteCard: true, id_vehicle: data.data.id_vehicle});
        break;
      }

    });
  }

  componentDidMount() {
    AsyncStorage.getItem("isSplashScreenShown").then((value) => {
              this.setState({isSplashScreenShown: value});
    }).done();
  }

  render() {
    const viewToShow = this.state.isSplashScreenShown ? <WebViewContainer /> : <ScreenSplash />;

    const HEADER = (
        <View style={styles.header}>
          {viewToShow}
        </View>
    );

    const PARALLAX = (
        <ParallaxView
            ref={component => this._scrollView = component}
            backgroundSource={{ }}
            windowHeight={500}
            header={HEADER}>
          <View style={styles.loremBody}>
              {this.state.showRouteCard && <RouteCard vehicle_id={this.state.id_vehicle}></RouteCard>}
              {this.state.showStationCard && <TransportCard id_station={this.state.id_station}></TransportCard>}
          </View>
        </ParallaxView>
    );

    var CONTENT;

    if (this.state.showStationCard || this.state.showRouteCard) {
      CONTENT = PARALLAX;
    } else {
      CONTENT = viewToShow;
    }

    return (
      <View style={styles.viewPort}>
        {CONTENT}
      </View>
    );
  }
}


const styles = StyleSheet.create({
  viewPort: {
    flex: 1,
    height: 200
  },
  header: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  loremBody: {
      paddingHorizontal: 10,
      paddingVertical: 6
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});


AppRegistry.registerComponent('eta', () => eta);
