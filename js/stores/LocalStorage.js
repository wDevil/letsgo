'use strict';

import { EventEmitter }  from 'events';

class LocalStorage extends EventEmitter {
	addToAsyncStorage(text) {
		this.emit('change');
	}

	msgFromMap(data) {
		console.log('msg in local storage: ' + data);
		this.emit('msgMap', data);
	}
}

const localStorage = new LocalStorage;

export default localStorage;