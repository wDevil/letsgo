'use strict';

var dgUrl = 'http://tile{s}.maps.2gis.com/tiles?x={x}&y={y}&z={z}&v=1',
    licenseAttrib = '<div class="dg-attribution leaflet-control">'+
        '<div class="dg-attribution__copyright"><ul class="dg-attribution__links">'+
        '<li class="dg-attribution__link-item">© <a href="http://www.openstreetmap.org/copyright" target="_blank" class="dg-attribution__link">OpenStreetMap contributors</a>, <a href="http://api.2gis.ru/?utm_source=copyright&amp;utm_medium=map&amp;utm_campaign=partners" target="_blank" class="dg-attribution__link">API 2ГИС</a></li><li class="dg-attribution__link-item"><a href="http://help.2gis.ru/licensing-agreement/" target="_blank" class="dg-attribution__link">Лицензионное соглашение</a></li></ul><a href="http://info.2gis.ru/?utm_source=copyright&amp;utm_medium=map&amp;utm_campaign=partners" target="_blank" class="dg-attribution__logo-url"></a></div></div>';
var dgLayer = L.tileLayer(dgUrl, {
        subdomains: '0123',
        maxZoom: 18,
        detectRetina: true,
        attribution: licenseAttrib}
    );

var api_host = 'http://eta2.navi.ostack.test';
var platform_url = api_host + '/platforms';
var platform_eta = api_host + '/platform_eta';
var vehicles_get = api_host + '/vehicles';

var map = new L.Map('map', {
        center: [59.93471, 30.314369],
        zoom: 14,
        markerZoomAnimation: false
    }).addLayer(dgLayer);

var stationsLayer;
var vehiclestLayer = L.featureGroup();
vehiclestLayer.on('click', function(e) {
    console.log('click on vehicle', e);
});


map.attributionControl.setPrefix('');
updateStationsLayer(map);
updateVehiclesLayer(map);

var vehicleTimer;

function periodicVehicleUpdate() {
    console.log('periodic update');
    if (vehiclestLayer._map) {
        updateVehiclesLayer(map);
    }
    vehicleTimer = setTimeout(periodicVehicleUpdate, 10*1000);
}

periodicVehicleUpdate(vehiclestLayer);

/*
 * <=============== STATIONS ==============>
 */

function make_geojson(data) {
    var stations = [];
    data.response.platforms.forEach(function(station) {
        stations.push(turf.point([station.location.lon, station.location.lat], {
            id: station.id,
            title: station.name,
            type: station.type
        }));
    });
    return turf.featurecollection(stations);
}

function fetchStations(map) {
    var bounds = map.getBounds();

    var req = {
        project: 'spb',
        bounding_box: {
            top_left: {
                lat: bounds.getNorth(),
                lon: bounds.getWest()
            },
            bottom_right: {
                lat: bounds.getSouth(),
                lon: bounds.getEast()
            }
        }
    };

    return axios.post(platform_url, req)
        .then(function(response) {
            return response.data;
        })
        .then(make_geojson)
}

function makeStattionMarker(point, latlng) {
    var type = 'bus';

    if (point.properties.type == 'metro' || point.properties.type == 'tram') {
            type = point.properties.type;
    }

    return new L.Marker(latlng, {
        icon: L.divIcon({
            iconSize: [20, 20],
            html: '<div class="' + type + '-station"></div>'
        })
    });
}

function updateStationsLayer(map) {
    //WebViewBridge.send('{"type": "stationCLick", "data":{"id_station": 9071}}');
    //return; // without stations
    fetchStations(map)
        .then(function(geojson) {
            var newLayer = L.geoJson(geojson, {
                pointToLayer: makeStattionMarker,
                filter: function(geo) {
                    return  geo.properties.type != 'metro';
                }
            });
            newLayer.on('click', function(e) {
                var prop = e.layer.feature.properties;
                var url = '';

                if(document.location.toString().search("#") != -1) {
                    url = document.location.toString().substr(0,document.location.toString().search("#"));
                } else {
                    url = document.location.toString();
                }
                document.location = url + '#evt={"type": "stationClick", "data":{"id_station": '+ prop.id +'}}'
                console.log('click', prop);
               // showBusByStation(prop.id);
            });
            newLayer.addTo(map);
            if (stationsLayer) {
                stationsLayer.remove();
            }
            stationsLayer = newLayer;
        })
        .catch(function(err) {
            if (!(err instanceof Error) && err.status == 404) {
                stationsLayer.remove();
                stationsLayer = null;
            }
        });
}

map.on('moveend', function(e) {
    console.log('move end', e);
    var _map = e.target;
    var zoom = _map.getZoom();

    if (zoom < 15) {
        if (stationsLayer) {
            stationsLayer.remove();
        }
    } else {
        updateStationsLayer(_map);
    }

    if (zoom < 13) {
        vehiclestLayer.remove();
        vehiclestLayer.clearLayers();
    } else {
        updateVehiclesLayer(_map);
    }
});

map.on('zoomstart', function() {
    vehiclestLayer.eachLayer(function(l) {
        if (l._anim) {
            l._anim._realstop = true;
            l._anim.stop();
        }
     });
});

map.on('zoomend', function() {
    vehiclestLayer.eachLayer(function(l) {
        if (l._anim) {
            l._anim._realstop = false;
            l._step = l._step > 0 ? l._step - 1 : l._step;
            next_step(l);
        }
    });
});

/*
 * <=============== BUS ==============>
 */

function fetchPlatformEta(id) {
    var req = {
        project: 'spb',
        platform_id: id
    };

    return axios.post(platform_eta, req)
        .then(function(response) {
            return response.data;
        })
        .then(function(data) {
            console.log(data);
        })
        //.then(make_geojson)
        .catch(function(err) {
            console.log('fetch error ' + err.message);
        });
}

function showBusByStation(id) {
    // fetch bus by station id;
    fetchPlatformEta(id);
    // show bus on map
    // moving buses on map
}

function fetchVehicles(map) {
    var bounds = map.getBounds();

    var req = {
        project: 'spb',
        bounding_box: {
            top_left: {
                lat: bounds.getNorth(),
                lon: bounds.getWest()
            },
            bottom_right: {
                lat: bounds.getSouth(),
                lon: bounds.getEast()
            }
        }
    };

    return axios.post(vehicles_get, req)
        .then(function(response) {
            return response.data;
        })
        .catch(function(err) {
            return Promise.reject(err);
        });
}

function makeVehicleMarker(point, latlng) {
    console.log(point.transport_type);
    var type = point.transport_type;
    return new L.Marker(latlng, {
        zIndexOffset: 1000,
        icon: L.divIcon({
            iconSize: [20, 20],
            html: '<div class="transport-icon big-zoom '+type+'"><div class="arrow-container"></div></div>'
        })
    });
}

function getVehicleIcon(v, zoom) {
    var cl = '';
    var size = [20, 20];
    var number = '';

    if (zoom <= 14) {
        cl += 'big-zoom ';
        size = [10, 10];
    }

    cl += v.transport_type;

    if (zoom >= 16) {
        cl += ' selected';
        size = [32, 32];
        number = '<div class="number">' + v.route_name + '</div>';
    }

    return L.divIcon({
        size: size,
        html: '<div class="transport-icon ' + cl + '">' + number + '<div class="arrow-container" style="transform:rotate(' + v.azimuth + 'deg)"></div></div>'
    });
}

function updateVehiclesLayer(map) {
    fetchVehicles(map)
        .then(function(data) {
            var newVehicles = [];
            // remove vehicles that not in response and not in viewport
            vehiclestLayer.eachLayer(function(l) {
                var found = false;

                data.response.vehicles.forEach(function(v) {
                    if (l.data.id == v.id) {
                        found = true;
                    }

                    /*if (l._icon) {
                        var pos = L.DomUtil.getPosition(l._icon);

                        if (pos.x > 0 && pos.y > 0) {
                            found = true;
                        }
                    }*/
                });

                if (!found) vehiclestLayer.removeLayer(l);
            });

            // find new or updated devices
            data.response.vehicles.forEach(function(v) {
                var found = false;

                vehiclestLayer.eachLayer(function(l) {
                    if (l.data.id == v.id) {
                        if(l.data.fixation_time == v.fixation_time) {
                            found = true;
                        } else {
                            console.log('need udpate', l.data, v);
                            vehiclestLayer.removeLayer(l);
                        }
                    }
                });

                if (!found) newVehicles.push(v);
            });
            return {response: {vehicles: newVehicles }};
        })
        .then(function(data) {
            console.log('newVehicles', data.response.vehicles.length, data);

            data.response.vehicles.forEach(function (v) {
                console.log('each vehicle', v);
                var layer = new L.Marker([v.location.lat, v.location.lon], {
                    zIndexOffset: 1000,
                    icon: getVehicleIcon(v, map.getZoom())
                });
                layer.data = v;
                layer.on('add', function() {
                    var l = this;
                    l._anim = new L.PosAnimation();
                    l._step = 0;
                    l._anim.on('end', function() {
                        if (!this._realstop) {
                            next_step(l);
                        }
                    });
                    l._anim.on('step', function() {
                        if (l._map) {
                            l._latlng = l._map.layerPointToLatLng(L.DomUtil.getPosition(l._icon));
                        }
                    });
                    next_step(l);
                });
                layer.on('remove', function() {
                    var l = this;
                    if (l._anim) {
                        l._anim.off();
                        l._anim.stop();
                    }
                });

                vehiclestLayer.addLayer(layer);
            });

            vehiclestLayer.eachLayer(function(l) {
                l.setIcon(getVehicleIcon(l.data, map.getZoom()));
            });

            vehiclestLayer.addTo(map);
        })
        .catch(function(err) {
            if (!(err instanceof Error) && err.status == 404) {
                vehiclestLayer.remove();
                vehiclestLayer.clearLayers();
            } else {
                console.log(err);
            }
        })
}

function next_step(vl) {

    if (!vl._map) return;

    var data = vl.data;
    var nextSegment = data.segments[vl._step];

    if (nextSegment) {
        var currPointPx = L.DomUtil.getPosition(vl._icon);
        var nextPointPx = currPointPx;
        var time = nextSegment.time;

        if (nextSegment.type == "passage") {
            var nextPoint = nextSegment.geometry[1];
            var distance = vl._latlng.distanceTo([nextPoint.lat, nextPoint.lon]);
            nextPointPx = vl._map.latLngToLayerPoint([nextPoint.lat, nextPoint.lon]);

            time = distance / nextSegment.speed;
        }

        if ('azimuth' in nextSegment) {
            var arrow = vl._icon.getElementsByClassName('arrow-container')[0];
            arrow.style['transform'] = 'rotate(' + nextSegment.azimuth + 'deg)';
        }
        
        vl._anim.run(vl._icon, nextPointPx, time , 1);
        vl._step += 1;
    }
}
/*
        var markers = new L.FeatureGroup();

        function populate() {
            for (var i = 0; i < 10; i++) {
                L.marker(getRandomLatLng(map)).addTo(markers);
            }
            return false;
        }

        markers.bindPopup("<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p><p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque.</p>").addTo(map);

        populate();
        L.DomUtil.get('populate').onclick = populate;
*/
        function logEvent(e) { console.log(e.type); }

        // map.on('click', logEvent);
        // map.on('contextmenu', logEvent);

        // map.on('movestart', logEvent);
        // map.on('move', logEvent);
        // map.on('moveend', logEvent);

        // map.on('zoomstart', logEvent);
        // map.on('zoomend', logEvent);
